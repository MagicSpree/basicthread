package com.company;

import com.company.threads.ChickenThread;
import com.company.threads.EggThread;
import com.company.unit.Util;

public class Main {

    public static void main(String[] args) {
        Util.print("Что появилось раньше?");
        findAnswer(new ChickenThread());
        findAnswer(new EggThread());
    }

    public static void findAnswer(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
